package edu.utexas.Gripka.wherewolf;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

public class GameLobbyActivity extends Activity {
	
	private void startMainGameActivity()
	{
		Intent intent = new Intent(this, MainGameActivity.class);
		startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_game_lobby);
		
		
		ArrayList<Player> arrayOfPlayers = new ArrayList<Player>();
		
		
		// Create the adapter to convert the array to views
		PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		
		
		arrayOfPlayers.add(new Player(1, "malevillager3", "Tom", 5));
		arrayOfPlayers.add(new Player(2, "malevillager3", "George", 3));
		arrayOfPlayers.add(new Player(3, "malevillager3", "Abigail", 1));
		arrayOfPlayers.add(new Player(4, "malevillager3", "Martha", 0));
		
		// Attach the adapter to a ListView
		

		ListView playerListView = (ListView) findViewById(R.id.players_list);
		
		playerListView.setAdapter(adapter);
		
		final Button start_button = (Button) findViewById(R.id.start_game);

    	View.OnClickListener start_game_listener = new View.OnClickListener() {
    	public void onClick(View v) {
    	startMainGameActivity();
    			}
    		};
    	start_button.setOnClickListener(start_game_listener);
		

}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_lobby, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_game_lobby,
					container, false);
			return rootView;
		}
	}

}

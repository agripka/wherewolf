package edu.utexas.Gripka.wherewolf;

public class Game {
	private String gameName;
	private String adminName;
	
	public Game( String gameName, String adminName) {
		this.gameName = gameName;
		this.adminName = adminName;
	}


	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
}
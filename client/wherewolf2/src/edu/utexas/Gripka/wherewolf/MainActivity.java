package edu.utexas.Gripka.wherewolf;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends Activity {

	private void startRegisterActivity()
	{
		Intent intent = new Intent(this, Register_Activity.class);
		startActivity(intent);
	}
	
	private void startLoginActivity()
	{
		Intent intent = new Intent(this, GameSelectionActivity.class);
		startActivity(intent);
	}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        

    	final Button register = (Button) findViewById(R.id.register_button);

    	View.OnClickListener jim = new View.OnClickListener() {
    	public void onClick(View v) {
    	startRegisterActivity();
    			}
    		};
    	register.setOnClickListener(jim);
    	
    	final Button sign_in = (Button) findViewById(R.id.sign_in_button);

    	View.OnClickListener sign_in_listener = new View.OnClickListener() {
    	public void onClick(View v) {
    	startLoginActivity();
    			}
    		};
    	sign_in.setOnClickListener(sign_in_listener);
    	
        /* 
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.container);
        
        Button button = new Button(this);
        button.setText(R.string.sign_in);
        
        layout.addView(button);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

}

package edu.utexas.Gripka.wherewolf;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;


public class GameSelectionActivity extends Activity {
	
	private void startJoinGameActivity()
	{
		Intent intent = new Intent(this, GameLobbyActivity.class);
		startActivity(intent);
	}
	
	private void startCreateGameActivity()
	{
		Intent intent = new Intent(this, CreateGameActivity.class);
		startActivity(intent);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_game_selection);
		
		final Button JoinGameButton = (Button) findViewById(R.id.join_game);

    	View.OnClickListener JoinGame = new View.OnClickListener() {
    	public void onClick(View v) {
    	startJoinGameActivity();
    			}
    		};
    	JoinGameButton.setOnClickListener(JoinGame);
    	
    	final Button CreateGameButton = (Button) findViewById(R.id.create_game);

    	View.OnClickListener CreateGame = new View.OnClickListener() {
    	public void onClick(View v) {
    	startCreateGameActivity();
    			}
    		};
    		
    	Log.i("ARG","ONCLICKLISTENER");
    	CreateGameButton.setOnClickListener(CreateGame);

		ArrayList<Game> allGames = new ArrayList<Game>();

		GameAdapter adapter = new GameAdapter(this, allGames);
		
		allGames.add(new Game( "examplegame1", "Jim"));
		allGames.add(new Game( "examplegame2", "Pam"));
		allGames.add(new Game( "examplegame3", "Toby"));
		allGames.add(new Game( "examplegame4", "Angela"));
		allGames.add(new Game( "examplegame5","Andy"));
		allGames.add(new Game( "examplegame6","Dwight"));
		
		ListView gameLV = (ListView) findViewById(R.id.games_list);
		gameLV.setAdapter(adapter);
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_selection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_game_selection,
					container, false);
			return rootView;
		}
	}

}

import sqlite3
import md5

class UserAlreadyExistsException( Exception ):
    
        def __init__(self,err):
            self.err = err
            
        def __str__( self):
            return 'Exception: ' + self.err



class WherewolfDao:
		
    def __init__(self):
        print('Created the DAO')
        self.conn = sqlite3.connect( 'wherewolf.db')  #creates connection do that db file

    def create_player( self, username, password, firstname, lastname):
        c = self.conn.cursor()
        c.execute('SELECT COUNT(*) from user WHERE username=?',(username,))
        n= int(c.fetchone()[0])
        
        if n == 0:
            #need hash
            hashedpass = md5.new(password).hexdigest()

            #s = 'INSERT INTO users VALUES (%s, %s, %s, %s )' % username, password, \
             #   firstname, lastname)
            #c.execute(s)
            #above chunks dont work because of security, can use sql to delete everything
	
            c.execute( 'INSERT INTO user (username, password, firstname, lastname) VALUES( ?,?,?,?)', (username, hashedpass, firstname, lastname ))
            self.conn.commit()
        else:
            raise UserAlreadyExistsException('{} user already exists'.format((username)))
            
            #question marks filed in with second tuple

    def checkpassword( self, username, password):
        """ return true if password checks out """
        c = self.conn.cursor()
        results = c.execute('SELECT password FROM user WHERE username=?',(username,))
        hashedpass = md5.new(password).hexdigest()
        return results.fetchone()[0] == hashedpass

   # def set_location(self, username, lat, lng):

    #def get_location( self, username):

    #def get_nearby(self, username):

    #def add_item( self, username, itemname):

    #def get_items( self,username):

    #def add_achievement(self, username, achievementname):

    #def get_achievements( self, username):

    #def set_dead( self, username):

    #def get_players( self, gamename):
        
    

if __name__ == "__main__":
    dao = WherewolfDao()
    try:
        dao.create_player( 'frdickerson','furry','Robert','Dickerson')
        print 'Created a new player!'
    except UserAlreadyExistsException as e:
        print 'e'
    except Exception:
        print 'General error happened'
    username = 'rfdickerson'
    correct_pass = 'furry'
    incorrect_pass = 'scaley'
    print('Logging in {} with {}'.format( username, correct_pass))
    print('Result {} '.format( dao.checkpassword( username, correct_pass )))
    print('Logging in {} with {} '.format( username, incorrect_pass))
    print('Result: {}'.format( dao.checkpassword( username, incorrect_pass)))
    #dao.checkpassword( 'rfdickerson','furry')


    #Check to see if it's attatched to table

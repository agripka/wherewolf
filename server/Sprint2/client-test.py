import requests
from datetime import date

hostname = 'http://localhost:5000'

def create_user(username, password):
    url = hostname + "/user"
    payload = {'username':'luke', 'password': 'r2d2'}
    r = requests.post(url, auth=(username, password),data=payload)
    response = r.text
    print r.text
    
def add_task(username, password, description, 
        due_date, category, priority):
    url = hostname + "/tasks"
    r = requests.post(url, auth=(username, password))
    print r.text
    
def get_tasks(username, password):
    url = hostname + "/tasks"
    r = requests.get(url, auth=(username, password))
    return r.json()
    
def set_completed(username, password, task_id):
    pass
    
if __name__ == "__main__":
    create_user('darthvader', 'sith')
    add_task('darthvader','sith', 'Destroy rebel alliance', 
        date(2200, 3, 20), 'work', 5)
    #tasks = get_tasks('darthvader', 'sith')
    #for task in tasks:
    #    print task["description"]
	
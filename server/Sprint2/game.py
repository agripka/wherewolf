from flask import Flask, request, jsonify
app = Flask(__name__)

import random, math

from wherewolfdao import WherewolfDao, UserAlreadyExistsException, NoUserExistsException, BadArgumentsException

'''* POST /register
will create a new user
payload (username, password, firstname, lastname)
returns:
 {'status': 'success'}
 failure conditions:
     user already exists with that username
     password too short'''
@app.route('/v1/register', methods = ["POST"])
def create_user():
	db = WherewolfDao("wherewolf",'postgres','')
	
	username = request.form['username']
	password = request.form['password']
	firstname = request.form['firstname']
	lastname = request.form['lastname']
	
	response = {}
	
	if len( password ) >= 6:
		try:
			db.create_user( username, password, firstname, lastname )
			response["status"] = "success"
			print( 'created a user called {}').format(username)
		except UserAlreadyExistsException:
			response["status"] = "failure"
		except Exception:
			response["status"] = "failure"
	else:
		response["status"] = "failure"
	
	return jsonify(response)


@app.route( '/v1/game' , methods = ['GET'] )
def get_game():
	response = {}
	response["status"] = "success"
	return jsonify(response)
'''
* POST /game
will create a new game, sets user to admin
returns:
 {'status': 'success',
     'results': {'game_id': 202}}
 failure conditions:
     bad auth
     already administering a game'''
@app.route( '/v1/game' , methods = ['POST'] )
def create_game():
	db = WherewolfDao("wherewolf",'postgres','')

	game_name = request.form['game_name']
	description = request.form['description']
	
	response = {}
	response["results"] = {}
	auth = request.authorization
	
	username = auth.username
	password = auth.password

	#check password
	if not db.check_password(username, password):
		response["status"]='failure: bad auth'
		response["results"]["game_id"]=0
		return jsonify(response)
	
	#check if the username is already an admin
	games = db.get_games()
	
	#If there are no games at all, create a game
	if not games:
		db.create_game( username, game_name )
		response["status"] = "created_game"
		
		#get the game_id for the created game
		only_game_id = db.get_games()[0]["game_id"]
		response["results"]["game_id"] = only_game_id 

		return jsonify(response)
	
	#get the gameid
	for game in games:
		admin_id = db.game_info( game["game_id"] )["admin_id"]
				
		#test if the admin is already the admin for another game
		username_test = db.get_user_id(username)
		
		if username_test == admin_id:
			response['status'] = 'bad auth already administering a game'
			response["results"]["game_id"] = "DNE"
			return jsonify( response )
	
	new_game_id = db.create_game( username, game_name )
	response["status"] = "created_game"
	response["results"]["game_id"] = new_game_id
	
	return jsonify(response)

'''* DELETE /game/<gameid>
will delete the game only if user is the admin
returns:
 {'status': 'success'}
 failure conditions:
     bad auth
     not admin of the game'''

'''@app.route( '/v1/game/<gameid>' , methods = ['DELETE'] )
def delete_game():	 
	db = WherewolfDao()'''

'''*POST /game/<game_id>/lobby
 is allowed to start a game, when they do
 the werewolves are set'''

@app.route( '/v1/game/<game_id>/start/lobby', methods = ['POST'])
def start_game( game_id):

	db = WherewolfDao("wherewolf",'postgres','')

	'''print db.get_players_in_game(1)
	response["status"] = "ok"
	return jsonify(response)
'''
	print "START GAME"
	print game_id

	auth = request.authorization

	username = auth.username
	password = auth.password

	#check authorization
	if not db.check_password( username, password):
		response["results"] = "failture: bad auth"
		return jsonify(response)

	response = {}

	#check to make sure the user is the admin
	admin_id = db.game_info(game_id)['admin_id']
	user_id = db.get_user_id(username)

	if int(admin_id) != int(user_id):

		response["results"] = "user is not admin"
		return jsonify(response)
	
	print 'game id test'
	print isinstance( game_id, str)
	db.set_game_status( game_id, 1)

	#Once the game is started, assigne werewolves
	#Get all the player_ids for the game
	print "game id: ", game_id
	players = db.get_players_in_game(game_id)

	#shuffle the players in the game 
	random.shuffle( players )

	print "players in game", players
	num_players = len(players)

	num_werewolves = math.ceil(0.3*num_players)

	for player in players:
		db.set_werewolf(player[0])

	response["results"] = "success"

	return jsonify(response)

'''* POST /game/{gameid}/lobby
  joins a current game
  returns:
  {'status': 'success'}
  failure conditions:
      bad auth
      already in another game
      game not in the lobby mode (started or ended)'''

 ########## ADD /lobby back in ###############

@app.route( '/v1/game/<game_id>/lobby', methods = ['POST'])
def join_game(game_id):
	db = WherewolfDao("wherewolf",'postgres','')

	print 'JOIN GAME IN GAME'

	auth = request.authorization

	username = auth.username
	password = auth.password

	response = {}

	#check authorization
	if not db.check_password( username, password):
		response["status"] = "failure: bad auth"
		return jsonify(response)


	player_game_ids = db.get_games_by_username( username)

	print( "PLAYER GAME IDS: ", player_game_ids)

	game_status = db.game_info(game_id)["status"]

	print "GAME STATUS GAME:", game_status

	#If the user doesn't belong to any games, let them join
	if not player_game_ids:

		if game_status != 0:
			db.join_game( username, game_id)
			response["status"] = "success"
		else:

			response["status"] = "game not in the lobby mode"

	else:
		response["status"] = "already in another game"

	return jsonify( response)

	#get all of the id's of games already being played

	#get all of the player id's
	#check if user is already in a game



'''* PUT /game/{gameid}
  reports to the server your current position
  payload (lat, lng)
  returns:
  {'status': 'success',
      'results':
        {'werewolfscent':
            [{'player_id': 30, 'distance': 20},
            {'playerid': 31, 'distance': 33}]}}'''
@app.route('/v1/game/<game_id>', methods = ["PUT"])  
def update_game(game_id):
	db = WherewolfDao("wherewolf",'postgres','')
	
	auth = request.authorization
	
	username = auth.username
	password = auth.password
	
	lat = request.form['lat']
	lng = request.form['lng']
	
	response = {}
	response["results"] = {}

	if not db.check_password(username, password):
		response["status"] = "failure"
		return jsonify(response)
	
	db.set_location(username,lat, lng)
	alive_nearby = db.get_alive_nearby(username, game_id, 50)
	response["results"] = alive_nearby

	response["status"] = "success"	
    
	return jsonify(response)


'''* GET /game/{gameid}
  reports the current information about the game'''
@app.route( '/v1/game/<game_id>', methods = ['GET'])
def game_info(game_id):
	db = WherewolfDao("wherewolf",'postgres','')

  	# how do we get the gameid from the url?
  	auth = request.authorization
  	username = auth.username
  	password = auth.password

  	if not db.check_password(username, password):
  		return jsonify({"status":"failure:bad auth"})

  	#get the info from the dao
	returned_game_info = db.game_info(int(game_id))

  	return jsonify( returned_game_info )


'''* POST /game/{gameid}/ballot
  casts a vote, can place multiple votes up until nightfall
  payload (target player)
  returns:
  {'status': 'success'}
  failure conditions:
      bad auth
      voting during night
      not in game'''
'''@app.route('/v1/game/{gameid}/ballot', methods = ['POST'])
def cast_vote():
	db = WherewolfDao()

	#get the variables from client
	player_id = request.form["player_id"]'''



'''* GET /game/{gameid}/ballot
  gets the current state of the ballet
  returns:
  {'status': 'success',
      results: [
      {'playerid': 30, 'votes': 2}
      {'playerid': 31, 'votes': 1}
      ]
  failure conditions:
      bad auth
      not in game'''
      
@app.route('/v1/game/<game_id>/ballot', methods = ['GET'])
def current_ballot(game_id):
	db = WherewolfDao("wherewolf",'postgres','')

	auth = request.authorization
  	username = auth.username
  	password = auth.password

  	vote_info = db.get_ballot(game_id)

  	response = {}

  	if not db.check_password(username, password):
  		response = {"status":"failure: bad auth", "results":"None"}
  		return jsonify( response)

  	response["status"] = "success"
  	response["results"] = vote_info

  	return jsonify(response)

'''* POST /game/{gameid}/attack
  attacks a villager
  returns:
  {'status': 'success',
      results:
      { 'summary': 'death',
        'combatant': 20 }
    }
  failure conditions:
      bad auth
      not in game
      player is werewolf under cooldown period of 30 minutes
      player is a villager'''
@app.route('/v1/game/<game_id>/attack', methods = ['POST'])
def attack(game_id):
	db = WherewolfDao("wherewolf",'postgres','')

	auth = request.authorization
	username = auth.username
	password = auth.password

	response = {}

	#check password
	if not db.check_password( username, password):
		response["status"] = "failure: bad auth"
		return jsonify(response)

	player_id = request.form['player_id']

	#check if the attacker IS a werewolf

	#Get werewolf stats
	werewolf_stats = db.get_werewolf_stats(username)
	print "WEREWOLF STATS", werewolf_stats

	#Get the villager stats
	villager_stats = db.get_villager_stats(player_id)

	# Get the villager's weapons and armor
	villager_username = db.get_username_from_player_id(player_id)
	villager_items = db.get_items(villager_username)

	# get villager HP and attack
	villager_HP = 10;
	villager_strength = 0;

	for item in villager_items:

		villager_HP += item["quantity"]*item["armor_rating"]
		villager_strength += item["quantity"]*item["damage"]

	temp_response = {"villager_HP": villager_HP, "villager_strength":villager_strength}

	#Get werewolf HP and attack strength from werewolf_stats

	# Do a while loop and continue attacking until villager_HP or werewolf_HP=0

	return jsonify(temp_response)



'''* POST /game/{gameid}/time
  allows the test script to change the time artificially
  payload (current_time)
  returns:
  {'status': 'success'}
'''

if __name__ == "__main__":
    app.run(debug=True)
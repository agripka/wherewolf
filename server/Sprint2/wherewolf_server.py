from flask import Flask, request, jsonify
app = Flask(__name__)

from wherewolfdao import WherewolfDAO, UserAlreadyExistsException, NoUserExistsException, BadArgumentException

'''* POST /register
will create a new user
payload (username, password, firstname, lastname)
returns:
 {'status': 'success'}
 failure conditions:
     user already exists with that username
     password too short'''
@app.route("/register")
def register( username, password, firstname, lastname):
    return "Hello World!"

if __name__ == "__main__":
    app.run() 
'''
* POST /game
will create a new game, sets user to admin
returns:
 {'status': 'success',
     'results': {'game_id': 202}}
 failure conditions:
     bad auth
     already administering a game

* DELETE /game/<gameid>
will delete the game only if user is the admin
returns:
 {'status': 'success'}
 failure conditions:
     bad auth
     not admin of the game

* POST /game/{gameid}/lobby
  joins a current game
  returns:
  {'status': 'success'}
  failure conditions:
      bad auth
      already in another game
      game not in the lobby mode (started or ended)

* PUT /game/{gameid}
  reports to the server your current position
  payload (lat, lng)
  returns:
  {'status': 'success',
      'results':
        {'werewolfscent':
            [{'player_id': 30, 'distance': 20},
            {'playerid': 31, 'distance': 33}]}}

* GET /game/{gameid}
  reports the current information about the game

* POST /game/{gameid}/ballot
  casts a vote, can place multiple votes up until nightfall
  payload (target player)
  returns:
  {'status': 'success'}
  failure conditions:
      bad auth
      voting during night
      not in game

* GET /game/{gameid}/ballot
  gets the current state of the ballet
  returns:
  {'status': 'success',
      results: [
      {'playerid': 30, 'votes': 2}
      {'playerid': 31, 'votes': 1}
      ]
  failure conditions:
      bad auth
      not in game

* POST /game/{gameid}/attack
  attacks a villager
  returns:
  {'status': 'success',
      results:
      { 'summary': 'death',
        'combatant': 20 }
    }
  failure conditions:
      bad auth
      not in game
      player is werewolf under cooldown period of 30 minutes
      player is a villager

* POST /game/{gameid}/time
  allows the test script to change the time artificially
  payload (current_time)
  returns:
  {'status': 'success'}
'''
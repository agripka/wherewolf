# Wherewolf game DAO
# Abstraction for the SQL database access.

import psycopg2
import md5


class UserAlreadyExistsException(Exception):
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err
        
class NoUserExistsException(Exception):
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err
        
class BadArgumentsException(Exception):
    """Exception for entering bad arguments"""
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err

class WherewolfDao:

    def __init__(self, dbname='wherewolf', pgusername='postgres', pgpasswd=''):
        self.dbname = dbname
        self.pgusername = pgusername
        self.pgpasswd = pgpasswd
        print ('connection to database {}, user: {}, password: {}'.format(dbname, pgusername, pgpasswd))

    def get_db(self):
        print self.pgusername, self.pgpasswd
        return psycopg2.connect(database='wherewolf',user='postgres',password='')

    def create_user(self, username, password, firstname, lastname):
        """ registers a new player in the system """
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            print 'CREATE USER DN', conn
            c.execute('SELECT COUNT(*) from gameuser WHERE username=%s',(username,))
            n = int(c.fetchone()[0])
            # print 'num of rfdickersons is ' + str(n)
            if n == 0:
                hashedpass = md5.new(password).hexdigest()
                c.execute('INSERT INTO gameuser (username, password, firstname, lastname) VALUES (%s,%s,%s,%s)', 
                          (username, hashedpass, firstname, lastname))
                conn.commit()
            else:
                raise UserAlreadyExistsException('{} user already exists'.format((username)) )
        
    def check_password(self, username, password):
        """ return true if password checks out """
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            sql = ('select password from gameuser where username=%s')
            c.execute(sql,(username,))
            hashedpass = md5.new(password).hexdigest()
            u = c.fetchone()
            if u == None:
                raise NoUserExistsException(username)
            # print 'database contains {}, entered password was {}'.format(u[0],hashedpass)
            return u[0] == hashedpass
        
    def set_location(self, username, lat, lng):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('update player set lat=%s, lng=%s '
                   'where player_id=(select current_player from gameuser '
                   'where username=%s)')
            cur.execute(sql, (lat, lng, username))
            conn.commit()


    def get_location(self, username):
        conn = self.get_db()
        result = {}
        with conn:
            c = conn.cursor()
            sql = ('select player_id, lat, lng from player, gameuser '
                   'where player.player_id = gameuser.current_player '
                   'and gameuser.username=%s')
            c.execute(sql, (username,))
            row = c.fetchone()
            result["playerid"] = row[0]
            result["lat"] = row[1]
            result["lng"] = row[2]
        return result

        
    def get_alive_nearby(self, username, game_id, radius): 
        ''' returns all alive players near a player '''
        conn = self.get_db()
        result = []
        with conn:
            c = conn.cursor()
            sql_location = ('select lat, lng from player, gameuser where '
                           'player.player_id = gameuser.current_player '
                           'and gameuser.username=%s')
            c.execute(sql_location, (username,))
            location = c.fetchone()

            if location == None:
                return result

            # using the radius for lookups now
            sql = ('select player_id, '
                   'earth_distance( ll_to_earth(player.lat, player.lng), '
                   'll_to_earth(%s,%s) ) '
                   'from player where '
                   'earth_box(ll_to_earth(%s,%s),%s) '
                   '@> ll_to_earth(player.lat, player.lng) '
                   'and game_id=%s '
                   'and is_werewolf = 0 '
                   'and is_dead = 0')

            # sql = ('select username, player_id, point( '
            #       '(select lng from player, gameuser '
            #       'where player.player_id=gameuser.current_player '
            #       'and gameuser.username=%s), '
            #       '(select lat from player, gameuser '
            #       'where player.player_id=gameuser.current_player '
            #       'and gameuser.username=%s)) '
            #       '<@> point(lng, lat)::point as distance, '
            #       'is_werewolf '
            #       'from player, gameuser where game_id=%s '
            #       'and is_dead=0 '
            #       'and gameuser.current_player=player.player_id '
            #       'order by distance')
            # print sql

            c.execute(sql, (location[0], location[1], 
                            location[0], location[1], 
                            radius, game_id))
            for row in c.fetchall():
                d = {}
                d["player_id"] = row[0]
                d["distance"] = row[1]
                #d["distance"] = row[1]
                #d["is_werewolf"] = row[2]
                result.append(d)
        return result
                   
        
    def add_item(self, username, itemname):
        conn = self.get_db()
        with conn:
            cur=conn.cursor()

            cmdupdate = ('update inventory set quantity=quantity+1'
                         'where itemid=(select itemid from item where name=%s)' 
                         'and playerid='
                         '(select current_player from gameuser where username=%s);')
            cmd = ('insert into inventory (playerid, itemid, quantity)' 
                   'select (select current_player from gameuser where username=%s) as cplayer,'
                   '(select itemid from item where name=%s) as item,' 
                   '1 where not exists' 
                   '(select 1 from inventory where itemid=(select itemid from item where name=%s)' 
                   'and playerid=(select current_player from gameuser where username=%s))')
            cur.execute(cmdupdate + cmd, (itemname, username, username, itemname, itemname, username))

            conn.commit()

 
    def remove_item(self, username, itemname):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('update inventory set quantity=quantity-1 where ' 
                   'itemid=(select itemid from item where name=%s) and ' 
                   'playerid=(select current_player from gameuser where username=%s);')
            cmddelete = ('delete from inventory where itemid=(select itemid from item where name=%s)' 
                         'and playerid=(select current_player from gameuser where username=%s) '
                         'and quantity < 1;')
            cur.execute(cmd + cmddelete, (itemname, username, itemname, username))
            conn.commit()

    def set_werewolf(self,player_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('UPDATE player set is_werewolf=1 where player_id=%s')
            cur.execute(cmd,(player_id,))
            conn.commit()

    def get_items(self, username):
        conn = self.get_db()
        items = []
        with conn:
            c = conn.cursor()
            sql = ('select item.name, item.description, quantity '
                   'from item, inventory, gameuser where '
                   'inventory.itemid = item.itemid and '
                   'gameuser.current_player=inventory.playerid and '
                   'gameuser.username=%s')
            c.execute(sql, (username,))
            for item in c.fetchall():
                d = {}
                d["name"] = item[0]
                d["description"] = item[1]
                d["quantity"] = item[2]
                d["damage"] = item[3]
                d["armor_rating"] = item[4]
                items.append(d)
        return items

    def get_username_from_player_id(self,player_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT username from gameuser where current_player=%s'''
            cur.execute(cmd,(player_id,))
            username = cur.fetchone()[0]
            return username
        
    def award_achievement(self, username, achievementname):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('insert into user_achievement (user_id, achievement_id, created_at) '
                   'values ((select user_id from gameuser where username=%s), '
                   '(select achievement_id from achievement where name=%s), now());')
            cur.execute(cmd, (username, achievementname))
            conn.commit()

        
    def get_achievements(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('select name, description, created_at from achievement, user_achievement '
                   'where achievement.achievement_id = user_achievement.achievement_id '
                   'and user_achievement.user_id = '
                   '(select user_id from gameuser where username=%s);')
            cur.execute(cmd, (username,))
            achievements = []
            for row in cur.fetchall():
                d = {}
                d["name"] = row[0]
                d["description"] = row[1]
                d["created_at"] = row[2]
                achievements.append(d)
        return achievements

    def set_dead(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('update player set is_dead=1 '
                   'where player_id='
                   '(select current_player from gameuser where username=%s);')
            cur.execute(cmd, (username,))
            conn.commit()


    def get_players(self, gameid):
        conn = self.get_db()
        players = []
        with conn:
            cur = conn.cursor()
            cmd = ('select player_id, is_dead, lat, lng, is_werewolf from player '
                   ' where game_id=%s;')
            cur.execute(cmd, (gameid,))
            for row in cur.fetchall():
                p = {}
                p["playerid"] = row[0]
                p["is_dead"] = row[1]
                p["lat"] = row[2]
                p["lng"] = row[3]
                p["is_werewolf"] = row[4]
                players.append(p)
        return players

    def get_games_by_username( self, username):
        #get all of the game_id's for a username
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT current_player from gameuser where username=%s'''
            cur.execute( cmd,( username,))
            current_player = cur.fetchone()[0]

            cmd = '''SELECT game_id from player where player_id=%s'''
            cur.execute( cmd,(current_player,))
            game_ids = cur.fetchone()
        return game_ids

    def get_players_in_game( self, game_id):
        # use the game_id to return a list of all the player_ids
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            print "DAO GAME ID", game_id
            cmd = '''SELECT player_id from player where game_id=%s'''
            cur.execute( cmd,(game_id,))
            player_ids = cur.fetchall()
        return player_ids

    def get_user_stats(self, username):
        pass
	    
        
    def get_player_stats(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT current_player from gameuser where username=%s'''
            cur.execute(cmd,(username,))
            player_id = cur.fetchone()
            cmd = '''SELECT stat_value from player_stat where player_id=%s'''
            cur.execute(cmd,(player_id,))
            stats = cur.fetchall()
        return stats
     

    def get_werewolf_stats( self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT current_player from gameuser where username=%s'''
            cur.execute(cmd,(username,))
            player_id = cur.fetchone()[0]
            cmd = '''SELECT stat_value from player_stat where player_id=%s'''
            cur.execute(cmd,(player_id,))
            stats = cur.fetchall()
        return stats

    def get_villager_stats(self,player_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT stat_name,stat_value from player_stat where player_id=%s'''
            cur.execute(cmd,(player_id,))
            stats = cur.fetchall()
            return stats

    def get_user_id(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT user_id FROM gameuser where username=%s'''
            cur.execute(cmd,(username,))
            user_id = cur.fetchone()[0]
        return user_id

    def get_ballot(self, game_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT * FROM vote WHERE game_id=%s'''
            cur.execute(cmd,(game_id,))
            vote_info = cur.fetchall()
        return vote_info
		
			
    # game methods    
    def join_game(self, username, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            print "JOIN GAME DB", conn
            cmd = ('INSERT INTO player ( is_dead, lat, lng, game_id) '
                   'VALUES ( %s, %s, %s, %s) returning player_id')
            cmd2 = ('update gameuser set current_player=%s where username=%s')
            cur.execute(cmd,( 0, 0, 0, gameid))
            cur.execute(cmd2, (cur.fetchone()[0], username));
            conn.commit()
	
    def leave_game(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd1 = '''UPDATE gameuser set current_player = null where username=%s'''
            cur.execute(cmd1, (username,)) 
            conn.commit()
	    
        
    def create_game(self, username, gamename):
        ''' returns the game id for that game '''
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('INSERT INTO game (admin_id, name) VALUES ( '
                   '(SELECT user_id FROM gameuser where username=%s), '
                   '%s) returning game_id')
            cur.execute(cmd,(username, gamename,))
            game_id = cur.fetchone()[0]
            conn.commit()
            return game_id


    def game_info(self, game_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT game_id, admin_id, status, name from game where game_id=%s'''
            cur.execute(cmd, (game_id,))
            row = cur.fetchone()
            d = {}
            d["game_id"] = str(row[0])
            d["admin_id"] = str(row[1])
            d["status"] = str(row[2])
            d["name"] = str(row[3])
            return d

    def get_games(self):
        conn = self.get_db()
        games = []
        with conn:
            cur = conn.cursor()
            cmd = ('SELECT game_id, name, status from game')
            cur.execute(cmd)
            for row in cur.fetchall():
                d = {}
                d["game_id"] = row[0]
                d["name"] = row[1]
                d["status"] = row[2]
                games.append(d)
        return games

            
    def set_game_status(self, game_id, status):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('UPDATE game set status=%s '
                   'where game_id=%s')
            cur.execute(cmd, (game_id, status))
        

    def vote(self, game_id, player_id, target_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('insert into vote '
                   '(game_id, player_id, target_id, cast_date) '
                   'values ( %s,'
                   '(select current_player from gameuser where username=%s), '
                   '(select current_player from gameuser where username=%s), '
                   'now())')
            cur.execute(sql, (game_id, player_id, target_id))
            conn.commit()
            
    
    def clear_tables(self):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('truncate gameuser cascade')
            c.execute('truncate player cascade')
            c.execute('truncate user_achievement cascade')
            conn.commit()

            
if __name__ == "__main__":
    dao = WherewolfDao('wherewolf','postgres','')
    print dao.get_players_in_game(1)
    print len( dao.get_players_in_game(1))
    dao.set_werewolf(3)
    dao.get_db()
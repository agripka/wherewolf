import client

def main():
	print( 'ADD CHARACTERS TO GAME')
	client.create_users()
	
	print( 'CREATE GAME' )
	client.create_game('michael','paper1','test game','this game is a test')
	
	print( 'JOIN GAME')
	client.join_game( 'dwight', 'paper1', 1)
	client.join_game('jim','paper1',1)
	client.join_game('pam','paper1',1)
	client.join_game('ryan','paper1',1)
	client.join_game('andy','paper1',1)
	client.join_game('angela','paper1',1)
	client.join_game('toby','paper1',1)

	print('START GAME')
	client.start_game('michael','paper1',1)

	print("Update GAME")
	client.update_game( 'michael','paper1','1', 70, 80)

	print("GAME INFO")
	client.game_info('michael', 'paper1', 1)

	print("CURRENT BALLOT")
	client.current_ballot('michael','paper1',1)

	print("ATTACK")
	client.attack('michael','paper1',1,2)

main()
package edu.utexas.Gripka.wherewolf;

import edu.utexas.Gripka.wherewolf.R;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class MainActivity extends Activity {

	private void startRegisterActivity()
	{
		Intent intent = new Intent(this, Register_Activity.class);
		startActivity(intent);
	}
	
	private void startLoginActivity()
	{
		Log.i("ARG","Starting SigninTask");
		new SigninTask().execute();
	}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        
    	final Button register = (Button) findViewById(R.id.register_button);

    	View.OnClickListener jim = new View.OnClickListener() {
    	@Override
		public void onClick(View v) {
    		Log.i("ARG","Starting Register Activity");
    	startRegisterActivity();
    			}
    		};
    	register.setOnClickListener(jim);
    	
    	final Button sign_in = (Button) findViewById(R.id.sign_in_button);

    	View.OnClickListener sign_in_listener = new View.OnClickListener() {
    	@Override
		public void onClick(View v) {
    	startLoginActivity();
    			}
    		};
    	sign_in.setOnClickListener(sign_in_listener);
    	
    	startService(new Intent(this,WherewolfService.class));
    	
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
    
    class SigninTask extends AsyncTask<Void, Integer, SigninResponse> {

        @Override
        protected SigninResponse doInBackground(Void... request) {

            final EditText nameTV = (EditText) findViewById(R.id.username_input);
            final EditText passTV = (EditText) findViewById(R.id.password_input);
            
            String username = nameTV.getText().toString();
            String password = passTV.getText().toString();
            
            SigninRequest signinRequest = new SigninRequest(username, password);
            
            return signinRequest.execute(new WherewolfNetworking());
            
        }

        @Override
    	protected void onPostExecute(SigninResponse result) {

            Log.v("TAG", "Signed in user has player id " + result.getPlayerID());
            
            //final TextView errorText = (TextView) findViewById(R.id.error_text);
            
            if (result.getStatus().equals("success")) {
         
                final EditText nameTV = (EditText) findViewById(R.id.username_input);
                final EditText passTV = (EditText) findViewById(R.id.password_input);
                
                Log.i("MAIN_ACTIVITY: password test", passTV.getText().toString());
                
                WherewolfPreferences pref = new WherewolfPreferences(MainActivity.this);
                pref.setCreds(nameTV.getText().toString(), passTV.getText().toString());

                Log.v("TAG", "Signing in");
                Intent intent = new Intent(MainActivity.this, GameSelectionActivity.class);
                startActivity(intent);

            } else {
                // do something with bad password
               Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();
            }

        }

        

    }


}

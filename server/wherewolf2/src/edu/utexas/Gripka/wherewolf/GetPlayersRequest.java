package edu.utexas.Gripka.wherewolf;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import edu.utexas.Gripka.wherewolf.BasicRequest.RequestType;

public class GetPlayersRequest extends BasicRequest{
	
	  public GetPlayersRequest (String username, String password)
	  {
	      super(username, password);
	  }
	  
	  /**
	  * Put the URL to your API endpoint here
	  */
	  @Override
	  public String getURL() {
	      return "/v1/get_players";
	  }

	  @Override
	  public List<NameValuePair> getParameters() {
	      return null;
	  }

	  @Override
	  public RequestType getRequestType() {
	      return RequestType.GET;
	  }

	  @Override
	  public GetPlayersResponse execute(WherewolfNetworking net) {
	 			 
	      try {
	          JSONObject jObject = net.sendRequest(this);
	          
	          String status = jObject.getString("status");
	          
	          if (status.equals("success"))
	          {
	        	  String games = jObject.getString("games");
	        	  Log.i("JOIN GAME REQUEST","was actually successful");
	              return new GetPlayersResponse("success", "successfully retrieved games",games);
	          } else {
	        	  
	              String errorMessage = jObject.getString("error");
	              return new GetPlayersResponse("failure", errorMessage);
	          }
	          
	      } catch (WherewolfNetworkException ex)
	      {
	    	  Log.i("REQUEST","could not communicate with server");
	          return new GetPlayersResponse("failure", "could not communicate with server.");
	      } catch (JSONException e) {
	    	  
	    	  Log.i("REQUEST","could not parse JSON");
	          return new GetPlayersResponse("failure", "could not parse JSON.");
	      }
	        
	  }


}

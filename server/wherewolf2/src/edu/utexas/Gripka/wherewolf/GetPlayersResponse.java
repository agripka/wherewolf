package edu.utexas.Gripka.wherewolf;

public class GetPlayersResponse extends BasicResponse{
	String Players;
	
	  public GetPlayersResponse(String status, String errorMessage) {
	      super(status, errorMessage);
	  }
	  
	  public GetPlayersResponse(String status, String errorMessage, String Players) {
	      super(status, errorMessage);
	      this.Players = Players;
	  }
	  
	  public String getPlayers()
	  {
	      return Players;
	  }
}

	

package edu.utexas.Gripka.wherewolf;

import android.util.Log;

public class SigninResponse extends BasicResponse {
	  
	  //Log.i("ARG","Sign in Response");
	  private int playerID = -1;

	  public SigninResponse(String status, String errorMessage) {
	      super(status, errorMessage);
	  }
	  
	  public SigninResponse(String status, String errorMessage, int playerID) {
	      super(status, errorMessage);
	      
	      this.playerID = playerID;
	  }

	  
	  public int getPlayerID()
	  {
	      return playerID;
	  }
	  
	}
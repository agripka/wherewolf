package edu.utexas.Gripka.wherewolf;

import edu.utexas.Gripka.wherewolf.MainActivity.SigninTask;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Build;

public class CreateGameActivity extends Activity {
	
	private void startGameSelectionActivity()
	{
		Log.i("ARG","Starting CreateGameTask");
		new CreateGameTask().execute();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_create_game);
		
		final Button create_button = (Button) findViewById(R.id.create_game);

    	View.OnClickListener jim = new View.OnClickListener() {
    	public void onClick(View v) {
    	startGameSelectionActivity();
    			}
    		};
    	create_button.setOnClickListener(jim);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_create_game,
					container, false);
			return rootView;
		}
	}
	
	class CreateGameTask extends AsyncTask<Void, Integer, CreateGameResponse> {

        @Override
        protected CreateGameResponse doInBackground(Void... request) {

            WherewolfPreferences pref = new WherewolfPreferences(CreateGameActivity.this);
        	
            final EditText game_nameTV = (EditText) findViewById(R.id.game_name);
            final EditText descriptionTV = (EditText) findViewById(R.id.game_description);
            
            String game_name = game_nameTV.getText().toString();
            String description = descriptionTV.getText().toString();
            
            CreateGameRequest CreateGameRequest = new CreateGameRequest(pref.getUsername(), pref.getPassword(),
            		game_name, description);
            
            Log.i("Create game stuff",game_name);
            Log.i("Create game stuff",description);
            return CreateGameRequest.execute(new WherewolfNetworking());
            
        }

        @Override
    	protected void onPostExecute(CreateGameResponse result) {
            
            //final TextView errorText = (TextView) findViewById(R.id.error_text);
            
            if (result.getStatus().equals("success")) {
         
                Log.v("TAG", "CreateGame to GameSelection");
                Intent intent = new Intent(CreateGameActivity.this, GameLobbyActivity.class);
                startActivity(intent);

            } else {
                // do something with bad password
               Toast.makeText(CreateGameActivity.this, "Error", Toast.LENGTH_LONG).show();
            }

        }
	}

}

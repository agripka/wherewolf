package edu.utexas.Gripka.wherewolf;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Register_Activity extends Activity {

	private void startLoginActivity()
	{
		new RegisterTask().execute();
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_register_);
		Log.i("ARG","Got to register activity");

		final Button button = (Button) findViewById(R.id.register_button);

		View.OnClickListener jim = new View.OnClickListener() {
			public void onClick(View v) {
				Log.i("REGISTER ACTIVITY","register was clicked");
				startLoginActivity();
			}
		};
		button.setOnClickListener(jim);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
			.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register_, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_register_,
					container, false);
			return rootView;
		}
	}

	class RegisterTask extends AsyncTask<Void, Integer, CreateUserResponse> {

		@Override
		protected CreateUserResponse doInBackground(Void... request) {

			final EditText nameTV = (EditText) findViewById(R.id.username_register);
			final EditText passTV = (EditText) findViewById(R.id.password_register);
			final EditText pass2TV = (EditText) findViewById(R.id.password_register2);
			final EditText firstnameTV = (EditText) findViewById(R.id.firstname_register);
			final EditText lastnameTV = (EditText) findViewById(R.id.lastname_register);

			Log.i(passTV.getText().toString(), pass2TV.getText().toString());

			if ((passTV.getText().toString()).equals(pass2TV.getText().toString())){
				String username = nameTV.getText().toString();
				String password = passTV.getText().toString();
				String firstname = firstnameTV.getText().toString();
				String lastname = lastnameTV.getText().toString();

				Log.i("Register_Activity","Before Create User Request is called");

				CreateUserRequest CreateUserRequest = new CreateUserRequest(username, password, firstname, lastname);

				Log.i("Register_Activity","After Create User Request is called");

				return CreateUserRequest.execute(new WherewolfNetworking());
			}else{

				TextView error = (TextView) findViewById(R.id.error_text);
				error.setText("Passwords did not match");
				return null;
			}

		}

		protected void onPostExecute(CreateUserResponse result) {

			Log.v("TAG", "Registered");
			Log.i("REGISTER_ACTIVITY",result.getStatus());

			if (result.getStatus().equals("success")) {

				final EditText nameTV = (EditText) findViewById(R.id.username_input);
				final EditText passTV = (EditText) findViewById(R.id.password_input);

				WherewolfPreferences pref = new WherewolfPreferences(Register_Activity.this);
				pref.setCreds(nameTV.getText().toString(), passTV.getText().toString());

				TextView error = (TextView) findViewById(R.id.error_text);
				error.setText("");

				Log.v("TAG", "Registering");
				Intent intent = new Intent(Register_Activity.this, MainActivity.class);
				startActivity(intent);

			} else {

				Log.i("ARG","Went to error");
				TextView error = (TextView) findViewById(R.id.error_text);
				error.setText(result.getErrorMessage());
			}

		}


	}

}

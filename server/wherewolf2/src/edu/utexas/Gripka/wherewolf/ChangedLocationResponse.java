package edu.utexas.Gripka.wherewolf;

import java.util.Calendar;

public class ChangedLocationResponse extends BasicResponse{		  

	public ChangedLocationResponse(String status, String errorMessage) {
		super(status, errorMessage);
	}

	public long getCurrentTime() {
		// TODO Auto-generated method stub
		Calendar c = Calendar.getInstance(); 
		int seconds = c.get(Calendar.SECOND);
		return seconds;
	}
}

package edu.utexas.Gripka.wherewolf;

public class CreateUserResponse extends BasicResponse {

	public CreateUserResponse(String status, String errorMessage) {
	      super(status, errorMessage);
	  }
}

package edu.utexas.Gripka.wherewolf;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;

public class MainGameActivity extends Activity {
	CircadianWidgetView circadianWidget;
	int currentTime;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main_game);
		
		Log.i("ARG","BEFORE ARRAY");
		
		ArrayList<Player> arrayOfPlayers = new ArrayList<Player>();
		
		
		// Create the adapter to convert the array to views
		PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		
		
		arrayOfPlayers.add(new Player(1, "malevillager3", "Tim", 3));
		arrayOfPlayers.add(new Player(2, "malevillager3", "Wes", 4));
		arrayOfPlayers.add(new Player(3, "malevillager3", "Megan", 2));
		arrayOfPlayers.add(new Player(4, "malevillager3", "Sherry", 0));
		arrayOfPlayers.add(new Player(4, "malevillager3", "Erin", 6));
		arrayOfPlayers.add(new Player(4, "malevillager3", "Ryan", 4));
		
		// Attach the adapter to a ListView

		ListView playerListView = (ListView) findViewById(R.id.players_list_main);
		
		playerListView.setAdapter(adapter);
		
		circadianWidget = (CircadianWidgetView) findViewById(R.id.circadian);
		Calendar cal = Calendar.getInstance();
		double currentTime = (cal.get(Calendar.HOUR_OF_DAY)*100)+cal.get(Calendar.MINUTE);
		double time = (double) currentTime/100;
		circadianWidget.changeTime(time);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main_game,
					container, false);
			return rootView;
		}
	}

}

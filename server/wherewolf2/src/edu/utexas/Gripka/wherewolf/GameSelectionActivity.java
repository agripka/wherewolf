package edu.utexas.Gripka.wherewolf;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;



public class GameSelectionActivity extends Activity {
	
	int clickedGameId;
	
	private void startJoinGameActivity()
	{
		Intent intent = new Intent(this, GameLobbyActivity.class);
		startActivity(intent);
	}
	
	private void startCreateGameActivity()
	{
		Intent intent = new Intent(this, CreateGameActivity.class);
		startActivity(intent);
	}
	protected void populateGames (String gamelist){
		System.out.println("got to games");
		String fixedString = gamelist.replaceAll("[{\\[\\]}]", "");
		System.out.println(fixedString);
		String[] gamesSplit = fixedString.split(",|:");
		
		ArrayList<Game> allGames = new ArrayList<Game>();

		GameAdapter adapter = new GameAdapter(this, allGames);
		
		Log.i("Game selection","called getgamestask");
		//new GetGamesTask().execute();
		
		
		for (int i = 0; i < gamesSplit.length; i = i +6){
			System.out.println("status is " + gamesSplit[i + 1] + " game id is " + gamesSplit[i + 3]
					+ " and finally " + gamesSplit[i + 5]);
			String gamenum= gamesSplit[i+3].replace(" ", "");
			allGames.add(new Game( gamesSplit[i + 1], Integer.parseInt(gamenum), gamesSplit[i + 5]));
		}
			
		final ListView gameLV = (ListView) findViewById(R.id.games_list);
		gameLV.setAdapter(adapter);
		
		gameLV.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?>parent, View view, int position, long id){
				Log.v("TAG","Selected game to join");
				
				Game clickedGame = (Game) gameLV.getItemAtPosition(position);
				
				Log.i("GAMESELECTION","after clickedGame");
				System.out.println("clicked game id: "+clickedGame.getGameId());
				System.out.println("game name is : "+clickedGame.getGameName());
				System.out.println("clicked game status: "+clickedGame.getGameStatus());
				clickedGameId = clickedGame.getGameId();
				
				Log.i("GAME SELECTION","BEfore task is called");
				new GameSelectionTask().execute();
				
				//Log.i("GAME_ID",clickedGameId);
		
			}
		});
		}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_game_selection);
		
		final Button JoinGameButton = (Button) findViewById(R.id.join_game);

    	View.OnClickListener JoinGame = new View.OnClickListener() {
    	public void onClick(View v) {
    	startJoinGameActivity();
    			}
    		};
    	JoinGameButton.setOnClickListener(JoinGame);
    	
    	final Button CreateGameButton = (Button) findViewById(R.id.create_game);

    	View.OnClickListener CreateGame = new View.OnClickListener() {
    	public void onClick(View v) {
    	startCreateGameActivity();
    			}
    		};
    		
    	Log.i("ARG","ONCLICKLISTENER");
    	CreateGameButton.setOnClickListener(CreateGame);

		/*ArrayList<Game> allGames = new ArrayList<Game>();

		GameAdapter adapter = new GameAdapter(this, allGames);
		
		Log.i("Game selection","called getgamestask");*/
		new GetGamesTask().execute();
		
		/*allGames.add(new Game( "examplegame1", "Jim", 5));
		allGames.add(new Game( "examplegame2", "Pam", 4));
		allGames.add(new Game( "examplegame3", "Toby", 3));
		allGames.add(new Game( "examplegame4", "Angela", 1));
		allGames.add(new Game( "examplegame5","Andy", 2));
		allGames.add(new Game( "examplegame6","Dwight", 6));*/
		
		
		//final ListView gameLV = (ListView) findViewById(R.id.games_list);
		//gameLV.setAdapter(adapter);
		
		/*gameLV.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?>parent, View view, int position, long id){
				Log.v("TAG","Selected game to join");
				
				Game clickedGame = (Game) gameLV.getItemAtPosition(position);
				clickedGameId = Integer.parseInt(clickedGame.getGameId());
				
				new GameSelectionTask().execute();
				
				Log.i("GAME_ID", Integer.toString(clickedGameId));
				
				//Intent joinGameIntent = new Intent(GameSelectionActivity.this, GameLobbyActivity.class);
				//startActivity(joinGameIntent);
			}
		});*/
			
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_selection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_game_selection,
					container, false);
			return rootView;
		}
	}
	
	class GameSelectionTask extends AsyncTask<Void, Integer, JoinGameResponse> {

        @Override
        protected JoinGameResponse doInBackground(Void... request) {
           
            WherewolfPreferences pref = new WherewolfPreferences(GameSelectionActivity.this);
            
            Log.i("GAME_SELECTION",pref.getUsername());
            Log.i("GAME_SELECTION", "password: "+pref.getPassword());
            
            JoinGameRequest JoinGameRequest = new JoinGameRequest(pref.getUsername(), 
            		pref.getPassword(),clickedGameId);
            	
            	Log.i("GAME_SELECTION","RIGHT BEFORE RETURN");
            	return JoinGameRequest.execute(new WherewolfNetworking());
        }
        
        

        protected void onPostExecute(JoinGameResponse result) {
        	
        	Log.i("SELECTION","GOT TO POST EXECUTE");

        	Log.i("SELECTION STATUS",result.getStatus());
        	Log.i("SELECTION MESSAGE",result.getErrorMessage());
            
            if (result.getStatus().equals("success")) {
                
                WherewolfPreferences pref = new WherewolfPreferences(GameSelectionActivity.this);
                pref.setCreds(pref.getUsername(), pref.getPassword());

                
                Log.v("TAG", "GameSelected");
                Intent intent = new Intent(GameSelectionActivity.this, MainGameActivity.class);
                startActivity(intent);

            } 

        }
	}
        class GetGamesTask extends AsyncTask<Void, Integer, GetGamesResponse> {

	        @Override
	        protected GetGamesResponse doInBackground(Void... request) {
	        	
	        	Log.i("GAME SELECTION","Got to do in Background");
	           
	            WherewolfPreferences pref = new WherewolfPreferences(GameSelectionActivity.this);
	            
	            Log.i("GAME_SELECTION",pref.getUsername());
	            Log.i("GAME_SELECTION", "password: "+pref.getPassword());
	            
	            GetGamesRequest GetGamesRequest = new GetGamesRequest(pref.getUsername(), 
	            		pref.getPassword());
	            	
	            	Log.i("GAME_SELECTION","RIGHT BEFORE RETURN");
	            	return GetGamesRequest.execute(new WherewolfNetworking());
	        }
	        
		

	        protected void onPostExecute(GetGamesResponse result) {

	        	Log.i("SELECTION STATUS",result.getStatus());
	        	Log.i("SELECTION MESSAGE",result.getErrorMessage());
	            
	            if (result.getStatus().equals("success")) {
	
	            	Log.i("Get games","get games was successful");
	            	String games = result.getGames();
	            	Log.i("GAMES STRING",games);
	            	
	            	populateGames(games);
	            	
	            }
	            } 

        }
        
	}



